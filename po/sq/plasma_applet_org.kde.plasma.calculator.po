# Albanian translation for kdeplasma-addons
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdeplasma-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-18 00:58+0000\n"
"PO-Revision-Date: 2009-05-01 03:23+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-22 02:23+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/calculator.qml:320
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr ""

#: package/contents/ui/calculator.qml:343
#, fuzzy, kde-format
#| msgctxt "The C button of the calculator"
#| msgid "C"
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:356
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr ""

#: package/contents/ui/calculator.qml:369
#, fuzzy, kde-format
#| msgctxt "The × button of the calculator"
#| msgid "×"
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:381
#, fuzzy, kde-format
#| msgctxt "The AC button of the calculator"
#| msgid "AC"
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:433
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr ""

#: package/contents/ui/calculator.qml:485
#, fuzzy, kde-format
#| msgctxt "The + button of the calculator"
#| msgid "+"
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:536
#, fuzzy, kde-format
#| msgctxt "The = button of the calculator"
#| msgid "="
msgctxt "Text of the equals button"
msgid "="
msgstr "="

#~ msgctxt "The − button of the calculator"
#~ msgid "−"
#~ msgstr "−"

#~ msgid "ERROR"
#~ msgstr "GABIM"

#~ msgid "ERROR: DIV BY 0"
#~ msgstr "GABIM: PJESËTIM ME 0"
