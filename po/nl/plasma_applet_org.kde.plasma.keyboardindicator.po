# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2018, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2022-05-07 18:06+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.0\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Sleutels"

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Wanneer geactiveerd tonen:"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:30
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Caps Lock geactiveerd\n"

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr "Num Lock geactiveerd\n"

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr "Geen lock-toetsen geactiveerd"

#~ msgid "Num Lock"
#~ msgstr "Num Lock"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Vergrendeld\n"

#~ msgid "Unlocked"
#~ msgstr "Ontgrendeld"

#~ msgid "Appearance"
#~ msgstr "Uiterlijk"
