# translation of plasma_packagestructure_comic.po to Italian
# Vincenzo Reale <smart2128vr@gmail.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_packagestructure_comic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2009-06-02 21:08+0100\n"
"Last-Translator: Vincenzo Reale <smart2128@baslug.org>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Immagini"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Script eseguibili"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "File script principale"
