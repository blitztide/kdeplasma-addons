# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin Weng <franklin@goodhorse.idv.tw>, 2012.
# pan93412 <pan93412@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-16 00:49+0000\n"
"PO-Revision-Date: 2018-04-23 00:55+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/ui/konsoleprofiles.qml:61
#, kde-format
msgctxt "@title"
msgid "Konsole Profiles"
msgstr "Konsole 設定檔"

#: package/contents/ui/konsoleprofiles.qml:81
#, kde-format
msgid "Arbitrary String Which Says Something"
msgstr "描述某東西的任意字串"
