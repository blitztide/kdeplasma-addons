# Frederik Schwarzer <schwarzer@kde.org>, 2008, 2009, 2016, 2022.
# Burkhard Lück <lueck@hube-lueck.de>, 2016, 2018, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_spellcheckrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-23 00:46+0000\n"
"PO-Revision-Date: 2022-03-13 10:34+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: spellcheck.cpp:98 spellcheck_config.cpp:65 spellcheck_config.cpp:96
#, kde-format
msgid "spell"
msgstr "spell"

#: spellcheck.cpp:103
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:104
#, kde-format
msgid "Checks the spelling of :q:."
msgstr "Überprüft die Rechtschreibung von :q:."

#: spellcheck.cpp:224
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "Richtig"

#: spellcheck.cpp:233
#, kde-format
msgid "Suggested term"
msgstr "Vorgeschlagenes Wort"

#: spellcheck.cpp:263
#, kde-format
msgid "No dictionary found, please install hspell"
msgstr "Es wurde kein Wörterbuch gefunden, bitte installieren Sie „hspell“"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "Einstellungen zur Rechtschreibprüfung"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "Schlagwo&rt benötigt"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "&Schlagwort:"

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, kde-format
msgid "Configure Dictionaries…"
msgstr "Wörterbücher einrichten ..."

#~ msgctxt "@action"
#~ msgid "Copy to Clipboard"
#~ msgstr "In Zwischenablage kopieren"

#~ msgid "Could not find a dictionary."
#~ msgstr "Es wurde kein Wörterbuch gefunden."

#~ msgctxt "separator for a list of words"
#~ msgid ", "
#~ msgstr ", "
