# translation of plasma_packagestructure_comic.po to Slovak
# Richard Fric <Richard.Fric@kdemail.net>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_packagestructure_comic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2009-05-07 21:03+0200\n"
"Last-Translator: Richard Fric <Richard.Fric@kdemail.net>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Obrázky"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Spustiteľné skripty"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "Hlavný skript súbor"
