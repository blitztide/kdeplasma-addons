# Translation of plasma_applet_notes into Japanese.
# This file is distributed under the same license as the kdeplasma-addons package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007, 2008, 2009.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-05 00:49+0000\n"
"PO-Revision-Date: 2021-10-17 14:02-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "外観"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "フォントサイズ:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "背景色"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "白色の付箋"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "黒色の付箋"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "赤色の付箋"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "オレンジ色の付箋"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "黄色の付箋"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "緑色の付箋"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "青色の付箋"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "ピンク色の付箋"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "半透明の付箋"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "明るい色のテキストの半透明の付箋"

#: package/contents/ui/main.qml:255
#, kde-format
msgid "Undo"
msgstr "取り消し"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "Redo"
msgstr "やり直し"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Cut"
msgstr "カット"

#: package/contents/ui/main.qml:281
#, kde-format
msgid "Copy"
msgstr "コピー"

#: package/contents/ui/main.qml:289
#, kde-format
msgid "Paste"
msgstr "貼り付け"

#: package/contents/ui/main.qml:295
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgid "Paste with Full Formatting"
msgstr "書式設定なしで貼り付け"

#: package/contents/ui/main.qml:302
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "書式設定なしで貼り付け"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Delete"
msgstr "削除"

#: package/contents/ui/main.qml:324
#, kde-format
msgid "Clear"
msgstr "クリア"

#: package/contents/ui/main.qml:334
#, kde-format
msgid "Select All"
msgstr "すべて選択"

#: package/contents/ui/main.qml:462
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "太字"

#: package/contents/ui/main.qml:474
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "斜体"

#: package/contents/ui/main.qml:486
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "下線"

#: package/contents/ui/main.qml:498
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "取り消し線"

#: package/contents/ui/main.qml:572
#, kde-format
msgid "Discard this note?"
msgstr "この付箋を破棄しますか？"

#: package/contents/ui/main.qml:573
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "本当にこの付箋を破棄しますか？"

#: package/contents/ui/main.qml:586
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "白"

#: package/contents/ui/main.qml:587
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "黒"

#: package/contents/ui/main.qml:588
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "赤"

#: package/contents/ui/main.qml:589
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "オレンジ"

#: package/contents/ui/main.qml:590
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "黄"

#: package/contents/ui/main.qml:591
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "緑"

#: package/contents/ui/main.qml:592
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "青"

#: package/contents/ui/main.qml:593
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "ピンク"

#: package/contents/ui/main.qml:594
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "半透明"

#: package/contents/ui/main.qml:595
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "明るい半透明"
