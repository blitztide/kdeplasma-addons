add_definitions(-DTRANSLATION_DOMAIN="plasma_runner_datetime")

kcoreaddons_add_plugin(org.kde.datetime SOURCES datetimerunner.cpp INSTALL_NAMESPACE "kf${QT_MAJOR_VERSION}/krunner")
target_link_libraries(org.kde.datetime
    KF5::Runner
    KF5::KIOWidgets
    KF5::I18n
)

if(BUILD_TESTING)
add_subdirectory(autotests)
endif()
